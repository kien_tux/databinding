import XCTest

import DataBindingTests

var tests = [XCTestCaseEntry]()
tests += DataBindingTests.allTests()
XCTMain(tests)
