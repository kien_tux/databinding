//
//  Observable.swift
//  SimpleTwoWayBinding
//
//  Created by Manish Katoch on 11/26/17.
//

import Foundation

public protocol LiveDataType {
    associatedtype T
    
    typealias ObserverType = (T) -> Void
    
    var value : T { get }
    
    func addObserver(observer: @escaping ObserverType)
}

public protocol MutableLiveDataType: LiveDataType {
    var value: T { get set }
}

public class LiveData<Element>: LiveDataType {
    public typealias T = Element
    
    internal var _value: Element {
        didSet {
            listeners.forEach { $0(_value) }
        }
    }
    
    public var value: Element {
        get {
            return _value
        }
    }
    
    private var listeners = [ObserverType]()
    
    public init(_ value: Element) {
        self._value = value
    }
    
    public func addObserver(observer: @escaping (Element) -> Void) {
        listeners.append(observer)
        observer(value)
    }
}

public class MutableLiveData<Element>: LiveData<Element>, MutableLiveDataType {
    override public var value: Element {
        get {
            return _value
        }
        set {
            self._value = newValue
        }
    }
    
    //    func set(value: Element) {
    //          self._value = value
    //      }
}

// MARK: - Element is Bool

extension MutableLiveData where Element == Bool {
    public func toggle() {
        value = !value
    }
}
