//
//  NSObject+Observable.swift
//  SimpleTwoWayBinding
//
//  Created by Manish Katoch on 11/26/17.
//

import Foundation

extension NSObject {
    public func observe<T>(for observable: LiveData<T>, callBack: @escaping (T) -> ()) {
        observable.addObserver { (value) in
            callBack(value)
        }
    }
}
