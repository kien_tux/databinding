//
//  BindingTwoWayProtocol.swift
//  Selectable
//
//  Created by ThangTM-PC on 11/22/19.
//  Copyright © 2019 ThangTM-PC. All rights reserved.
//

import UIKit

public protocol BindindTwoWay: class {
    associatedtype BindingType
    var defaultValue: BindingType {get}
    var observingValue : BindingType {get set}
}

// MARK: - BindindTwoWay
fileprivate struct AssociatedKeys {
    static var binder: UInt8 = 0
}

extension BindindTwoWay {
    
    private var binder: MutableLiveData<BindingType> {
        get {
            guard let value = objc_getAssociatedObject(self, &AssociatedKeys.binder) as? MutableLiveData<BindingType> else {
                let newValue = MutableLiveData<BindingType>(defaultValue)
                objc_setAssociatedObject(self, &AssociatedKeys.binder, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
                return newValue
            }
            return value
        }
        set(newValue) {
            objc_setAssociatedObject(self, &AssociatedKeys.binder, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    public func bind(with liveData: MutableLiveData<BindingType>) {
        binder = liveData
        if let _self = self as? UIControl {
            
            _self.addTarget(Selector, action: Selector{ [weak self] in
                if let self = self {
                    // View to ViewModel
                    // Emit value to ViewModel
                    self.binder.value = self.observingValue
                }
            }, for: [.editingChanged, .valueChanged])
        }
        // VM to View
        binder.addObserver { (value) in
            // self.text = value
            self.observingValue = value
        }
    }
}
