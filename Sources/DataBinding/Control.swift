//
//  File.swift
//  
//
//  Created by ThangTM-PC on 11/21/19.
//

import UIKit

extension UILabel{
    public func bind(to livedata: LiveData<String?>) {
        livedata.addObserver { text in
            self.text = text
        }
    }
}


extension UISwitch {
    public func bind(to livedata: LiveData<Bool?>) {
        livedata.addObserver { (isOn:Bool?) in
            self.isOn = isOn ?? false
        }
    }
}

extension UISlider {
    public func bind(to livedata: LiveData<Float>) {
        livedata.addObserver { (value: Float) in
            self.value = value
        }
    }
}


extension UIStepper {
    public func bind(to livedata: LiveData<Double>) {
        livedata.addObserver { (value: Double) in
            self.value = value
        }
    }
}

extension UISegmentedControl {
    public func bind(to livedata: LiveData<Int>) {
        livedata.addObserver { (selectedSegmentIndex) in
            self.selectedSegmentIndex = selectedSegmentIndex
        }
    }
}
