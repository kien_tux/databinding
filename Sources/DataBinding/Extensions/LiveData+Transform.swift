//
//  File.swift
//  
//
//  Created by Kien Nguyen on 11/29/19.
//

import Foundation

class LiveDataTransformations {
    static func map<S, E>(_ source: LiveData<S>, transform: @escaping (S) -> E) -> LiveData<E> {
        let combinedLiveData = CombinedLiveData(transform(source.value))
        combinedLiveData.addSource(source) { e in
            combinedLiveData.value = transform(e)
        }
        return combinedLiveData
    }
    
    static func distinctUntilChanged<E>(_ source: LiveData<E>) -> LiveData<E> where E: Equatable {
        let combinedLiveData = CombinedLiveData(source.value)
        combinedLiveData.addSource(source) { e in
            if e != combinedLiveData.value {
                combinedLiveData.value = e
            }
        }
        return combinedLiveData
    }
}

public extension LiveData {
    func map<T>(transform: @escaping (Element) -> T) -> LiveData<T> {
        return LiveDataTransformations.map(self, transform: transform)
    }
}

public extension LiveData where Element: Equatable {
    func untilChanged() -> LiveData<Element> {
        return LiveDataTransformations.distinctUntilChanged(self)
    }
}
